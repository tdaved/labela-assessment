# SWAPI implementation

Create an app with the [Star Wars API](https://swapi.co/) that has:

* A view that shows all categories of the API
* A view that shows the items in a list that is searchable and sortable without the use of external libraries.

## Requirements

- [x] Modular styling
- [x] Webpack
- [x] React with ES6+
- [x] Redux

## Bonus

- [x] Serverside rendering
- [x] Styled-Components
- [] Unit tests
- [] Add Wookie language toggler