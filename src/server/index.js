import '@babel/polyfill';
import React from 'react';
import express from 'express';
import cors from 'cors';
import {renderToString} from 'react-dom/server';
import {StaticRouter} from 'react-router-dom';
import {Provider as ReduxProvider} from 'react-redux';
import {ServerStyleSheet} from 'styled-components';
import {App} from '~/shared/components/App';
import {Html} from '~/shared/components/Html';
import {fetchCategories, fetchCategoryItems} from '~/shared/components/Api';
import createStore from '~/shared/store';
import {saveData} from '~/shared/actions';

const port = 3000;
const server = express();

server.use(cors());
server.use(express.static('public'));

const store = createStore();

/*
 * Categories
 *
 * Fetches categories and sends it through redux. When fetching
 * the categories page, we don't need anything else rather than
 * the categories.
 */
server.get('/', async (req, res) => {
    const categories = await fetchCategories();
    store.dispatch(saveData('categories', categories));

    const sheet = new ServerStyleSheet();
    const body = renderToString(
        sheet.collectStyles(
            <ReduxProvider store={store}>
                <StaticRouter location={req.url} context={{}}>
                    <App />
                </StaticRouter>
            </ReduxProvider>
        )
    );
    const styles = sheet.getStyleTags();

    res.send(Html({body, styles, reduxData: store.getState()}));
});

/*
 * Category details
 *
 * Fetches items for the category in a paginated format. Then
 * sends it through redux to the client.
 */
server.get('/:categoryId', async (req, res) => {
    const {categoryId} = req.params;
    const categoryItems = await fetchCategoryItems(categoryId);
    store.dispatch(saveData(categoryId, categoryItems));

    const sheet = new ServerStyleSheet();
    const body = renderToString(
        sheet.collectStyles(
            <ReduxProvider store={store}>
                <StaticRouter location={req.url} context={{}}>
                    <App />
                </StaticRouter>
            </ReduxProvider>
        )
    );
    const styles = sheet.getStyleTags();

    res.send(Html({body, styles, reduxData: store.getState()}));
});

server.listen(port, () => {
    console.log(`🌍 Serving at http://localhost:${port}`);
});