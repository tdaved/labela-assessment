export default {
    english: {
        label: 'English',
        code: 'en'
    },
    wookie: {
        label: 'Wookie',
        code: 'wo'
    }
};