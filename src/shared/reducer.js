import {CHANGE_LANGUAGE, DELETE_DATA, SAVE_DATA} from '~/shared/constants';
import languages from '~/shared/languages';

export const initialState = {
    language: languages.english.code
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case SAVE_DATA:
            return {
                ...state,
                [action.payload.key]: action.payload.value
            };
        case DELETE_DATA:
            const newState = {...state};
            delete newState[action.payload];
            return newState;
        case CHANGE_LANGUAGE:
            return {
                ...state,
                language: action.payload
            };
        default:
            return state;
    }
};

export default reducer;