import {SAVE_DATA, DELETE_DATA, CHANGE_LANGUAGE} from '~/shared/constants';

export const saveData = (key, value) => ({
    type: SAVE_DATA,
    payload: {key, value}
});

export const deleteData = (key) => ({
    type: DELETE_DATA,
    payload: key
});

export const changeLanguage = (code) => ({
    type: CHANGE_LANGUAGE,
    payload: code
});
