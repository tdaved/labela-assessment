import styled from 'styled-components';

const SearchStyleContainer = styled.div`
    display: flex;
    width: 100%;
    margin-bottom: 1rem;
    span {
        display: inline-block;
        margin-right: 1rem;
    }
`;

export default SearchStyleContainer;