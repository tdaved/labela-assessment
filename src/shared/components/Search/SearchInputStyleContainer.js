import styled from 'styled-components';

const SearchInputStyleContainer = styled.input`
    width: 100%;
    font-size: 1rem;
    padding: 0.125rem;
    border: none;
    border-radius: 0;
    border-bottom: 1px solid rgba(0, 0, 0, 0.38);
    &:focus {
        outline: none;
        border-bottom: 1px solid rgba(0, 0, 0, 0.60);
    }
`;

export default SearchInputStyleContainer;