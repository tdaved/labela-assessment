import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {saveData, deleteData} from '~/shared/actions';
import {fetchSearchResults} from '~/shared/components/Api';
import SearchStyleContainer from './SearchStyleContainer';
import SearchInputStyleContainer from './SearchInputStyleContainer';
import debounce from './debounce';

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentWillMount() {
        this.search = debounce((value) => {
            if (value) {
                fetchSearchResults(this.props.categoryId, value).then((response) => {
                    this.props.dispatch(saveData('search', response));
                });
            } else {
                this.props.dispatch(deleteData('search'));
            }
        }, 500);
    }

    componentWillUnmount() {
        if (this.props.searched) {
            this.props.dispatch(deleteData('search'));
        }
    }

    handleInputChange(event) {
        const value = event.target.value;

        this.setState({value});
        this.search(value);
    }

    render() {
        return (
            <SearchStyleContainer>
                <span>Search:</span>
                <SearchInputStyleContainer
                    type="search"
                    value={this.state.value}
                    onChange={this.handleInputChange}
                />
            </SearchStyleContainer>
        );
    }
}

Search.propTypes = {
    searched: PropTypes.bool.isRequired,
    categoryId: PropTypes.string.isRequired,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
    searched: !!state.search
});

export default connect(mapStateToProps)(Search);
