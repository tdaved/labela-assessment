import React from 'react';
import {HeaderElementStyleContainer} from '~/shared/components/Header';

const LanguageLabel = ({label, selected, onClick}) => (
    <HeaderElementStyleContainer
        onClick={onClick}
        bold={selected}
    >
        {label}
    </HeaderElementStyleContainer>
);

export default LanguageLabel;
