import React from 'react';
import {connect} from 'react-redux';
import languages from '~/shared/languages';
import {changeLanguage} from '~/shared/actions';
import LanguageLabel from '~/shared/components/LanguageSwitcher/LanguageLabel';
import LanguageSwitcherStyleContainer from './LanguageSwitcherStyleContainer';

class LanguageSwitcher extends React.Component {
    state = {
        selected: languages.english
    };

    handleSelect(languageCode) {
        return () => {
            this.setState({
                selected: languageCode
            });
            this.props.dispatch(changeLanguage(languageCode));
            console.info('Implementation is missing due to time... but could have been fun!')
        }
    }

    render() {
        return (
            <LanguageSwitcherStyleContainer>
                {Object.keys(languages).map((languageKey) => {
                    const language = languages[languageKey];
                    return (
                        <LanguageLabel
                            key={language.code}
                            label={language.label}
                            onClick={this.handleSelect(language.code)}
                            selected={language.code === this.props.selectedLanguageCode}
                        />
                    )
                })}
            </LanguageSwitcherStyleContainer>
        );
    }
}

LanguageSwitcher.propTypes = {};

const mapStateToProps = (state) => ({
    selectedLanguageCode: state.language
});

export default connect(mapStateToProps)(LanguageSwitcher);
