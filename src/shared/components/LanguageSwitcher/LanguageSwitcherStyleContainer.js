import styled from 'styled-components';

const LanguageSwitcherStyleContainer = styled.div`
    display: flex;
`;

export default LanguageSwitcherStyleContainer;