import '~/assets/styles/globalStyles';
import React from 'react';
import {Switch, Route} from 'react-router-dom';
import routes from '~/shared/routes';
import {Header} from '~/shared/components/Header';
import {Grid} from '~/shared/components/Grid';
import AppStyledContainer from './AppStyledContainer';

const App = () => (
    <>
        <Header />
        <Grid>
            <AppStyledContainer>
                <Switch>
                    {routes.map(route => <Route key={route.name} path={route.path} exact={route.exact} component={route.component} />)}
                </Switch>
            </AppStyledContainer>
        </Grid>
    </>
);

export default App;