import React from 'react';
import styled from 'styled-components';

const AppStyledContainer = styled.div`
    padding: 1rem;
    border-radius: 3px;
    background-color: #ffffff;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
`;

export default AppStyledContainer;