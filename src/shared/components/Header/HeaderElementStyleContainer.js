import styled from 'styled-components';
import PropTypes from 'prop-types';

const HeaderElementStyleContainer = styled.div`
    font-size: 0.8rem;
    font-weight: ${props => props.bold ? 'bold' : 'normal'};
    letter-spacing: 2px;
    text-transform: uppercase;
    cursor: pointer;
    color: ${props => props.bold ? 'rgba(0, 0, 0, .87)' : 'rgba(0, 0, 0, .38)'};
    &:not(:last-child) {
        margin-right: 1rem;
    }
    &:hover {
        color: rgba(0, 0, 0, .87)
    }
    & > a {
        text-decoration: none;
        color: ${props => props.bold ? 'rgba(0, 0, 0, .87)' : 'rgba(0, 0, 0, .38)'};
    }
`;

HeaderElementStyleContainer.propTypes = {
    bold: PropTypes.bool
};

HeaderElementStyleContainer.defaultProps = {
    bold: false
};

export default HeaderElementStyleContainer;