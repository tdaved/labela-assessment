import styled from 'styled-components';

const HeaderStyledContainer = styled.header`
    display: flex;
    justify-content: space-between;
    width: 100%;
    padding: 1rem;
    margin-bottom: 2rem;
`;

export default HeaderStyledContainer;