import React from 'react';
import {Title} from '~/shared/components/Title/index';
import {LanguageSwitcher} from '~/shared/components/LanguageSwitcher/index';
import HeaderStyledContainer from './HeaderStyleContainer';

const Header = () => (
    <HeaderStyledContainer>
        <Title />
        <LanguageSwitcher />
    </HeaderStyledContainer>
);

export default Header;
