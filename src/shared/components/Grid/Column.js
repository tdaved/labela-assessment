import PropTypes from 'prop-types';
import styled from 'styled-components';

const Column = styled.div`
    flex: 1 0 auto;
    padding-right: 1rem;
    padding-left: 1rem;
    ${props => props.shrink && 'flex: none;'}
`;

Column.displayName = 'Column';

Column.propTypes = {
    shrink: PropTypes.bool
};

Column.defaultProps = {
    shrink: false
};

export default Column;