import styled from 'styled-components';

const Grid = styled.div`
  max-width: 48rem;
  width: 100%;
  margin: 0 auto;
  padding-right: 1rem;
  padding-left: 1rem;
`;

Grid.displayName = 'Grid';

export default Grid;