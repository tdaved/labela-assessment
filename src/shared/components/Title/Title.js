import React from 'react';
import {Link} from 'react-router-dom';
import {HeaderElementStyleContainer} from '~/shared/components/Header';

const Title = () => (
    <HeaderElementStyleContainer bold>
        <Link to="/">SWAPI</Link>
    </HeaderElementStyleContainer>
);

export default Title;
