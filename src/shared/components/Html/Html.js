import PropTypes from 'prop-types';

const Html = ({body, styles = '', reduxData = {}}) => `
<!DOCTYPE html>
<html>
    <head>
        <title>Swapi implementation</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        ${styles}
        <script src="/bundle.js" defer></script>
        <script>window.__REDUX_DATA__ = ${JSON.stringify(reduxData)}</script>
    </head>
    <body style="margin:0">
        <div id="app">${body}</div>
    </body>
</html>
`;

Html.propTypes = {
    body: PropTypes.string.isRequired,
    styles: PropTypes.string,
    reduxData: PropTypes.object
};

Html.defaultProps = {
    styles: '',
    reduxData: {}
};

export default Html;