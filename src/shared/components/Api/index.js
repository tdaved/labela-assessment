import fetch from 'isomorphic-fetch';

const baseUrl = 'https://swapi.co/api';

export const fetchCategories = () => (
    fetch(baseUrl).then((response) => response.json()).then((data) => Object.keys(data))
);

export const fetchCategoryItems = (categoryId) => (
    fetch(`${baseUrl}/${categoryId}`).then((response) => response.json())
);

export const fetchSearchResults = (categoryId, searchQuery) => (
    fetch(`${baseUrl}/${categoryId}?search=${encodeURI(searchQuery)}`).then((response) => response.json())
);
