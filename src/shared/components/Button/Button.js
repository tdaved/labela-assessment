import styled from 'styled-components';

const Button = styled.button`
    padding: 0.5rem 2rem;
    font-size: 1rem;
    border-radius: 3px;
    border: none;
    box-shadow: none;
    cursor: pointer;
    color: #ffffff;
    background-color: #83ba23;
    &:hover {
        background-color: #72a912;
    }
`;

export default Button;