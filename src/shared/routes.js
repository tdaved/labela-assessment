import {CategoryList} from '~/shared/screens/CategoryList';
import {CategoryItemList} from '~/shared/screens/CategoryItemList';
import {NotFound} from '~/shared/screens/NotFound';

const routes = [
    {
        name: 'Categories',
        path: '/',
        exact: true,
        component: CategoryList
    },
    {
        name: 'Category item',
        path: '/:categoryId',
        component: CategoryItemList
    },
    {
        name: 'Boom! Not found.',
        component: NotFound
    }
];

export default routes;