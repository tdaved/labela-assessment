import {createStore as createReduxStore, compose} from 'redux';
import reducer, {initialState as initialReduxState} from '~/shared/reducer';

export default (initialState = initialReduxState) => {
    const middlewares = [];

    if (__isBrowser__ && window.__REDUX_DEVTOOLS_EXTENSION__) {
        middlewares.push(window.__REDUX_DEVTOOLS_EXTENSION__());
    }

    return createReduxStore(reducer, initialState, compose(...middlewares));
};
