import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import CategoryListItemStyleContainer from './CategoryListItemStyleContainer';

const categoryToIconMap = (categoryName) => {
    const map = {
        people: 'user',
        films: 'film',
        starships: 'location-arrow',
        vehicles: 'car',
        planets: 'globe-africa',
        species: 'dove',
        default: 'circle'
    };

    return map[categoryName] || map.default;
};

const CategoryListItem = ({categoryName}) => (
    <CategoryListItemStyleContainer>
        <Link to={`/${categoryName}`}>
            <i className={`fa fa-${categoryToIconMap(categoryName)}`} />
            <span>{categoryName}</span>
        </Link>
    </CategoryListItemStyleContainer>
);

CategoryListItem.propTypes = {
    categoryName: PropTypes.string.isRequired
};

export default CategoryListItem;