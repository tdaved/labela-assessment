import styled from 'styled-components';

const CategoryListItemStyleContainer = styled.li`
    list-style: none;
    a {
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 0.5rem;
        text-decoration: none;
        text-transform: capitalize;
        color: rgba(0, 0, 0, 0.60);
        &:hover {
            color: rgba(0, 0, 0, 0.87);
        }
    }
    i {
        font-size: 2rem;
        margin-bottom: 0.5rem;
    }
`;

export default CategoryListItemStyleContainer;