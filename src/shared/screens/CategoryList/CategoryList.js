import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {fetchCategories} from '~/shared/components/Api';
import {saveData} from '~/shared/actions';
import CategoryListStyleContainer from './CategoryListStyleContainer';
import CategoryListItem from './components/CategoryListItem';

class CategoryList extends React.Component {
    static data = fetchCategories;

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        const {categories} = this.props;

        if (__isBrowser__ && !categories) {
            CategoryList.data().then((response) => {
                this.props.dispatch(saveData('categories', response));
            });
        }
    }

    render() {
        const {categories} = this.props;

        if (categories) {
            return (
                <CategoryListStyleContainer>
                    {categories.map((categoryName) => <CategoryListItem key={categoryName} categoryName={categoryName}/>)}
                </CategoryListStyleContainer>
            );
        }

        return null;
    };
}

CategoryList.propTypes = {
    categories: PropTypes.array,
    dispatch: PropTypes.func.isRequired
};

CategoryList.defaultProps = {
    categories: null
};

const mapStateToProps = (state) => ({
    categories: state.categories
});

export default connect(mapStateToProps)(CategoryList);

