import styled from 'styled-components';

const CategoryListStyleContainer = styled.ul`
    display: flex;
    justify-content: space-between;
    width: 100%;
    margin: 0;
    padding: 1rem;
    @media (max-width: 48rem) {
        flex-direction: column;
    }
`;

export default CategoryListStyleContainer;