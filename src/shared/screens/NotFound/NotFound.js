import React from 'react';
import styled from 'styled-components';

const NotFoundStyleContainer = styled.div`

`;

const NotFound = () => {
    return (
        <NotFoundStyleContainer>
            <span>(╯°□°)╯︵ ┻━┻</span>
        </NotFoundStyleContainer>
    );
};

export default NotFound;
