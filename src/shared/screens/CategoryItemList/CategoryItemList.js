import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {fetchCategoryItems} from '~/shared/components/Api';
import {deleteData, saveData} from '~/shared/actions';
import {Row, Column} from '~/shared/components/Grid';
import {Search} from '~/shared/components/Search';
import CategoryItem from './components/CategoryItem';
import CategoryNavigation from './components/CategoryNavigation';

class CategoryItemList extends React.Component {
    static data = fetchCategoryItems;

    componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate(prevProps) {
        if (this.props.categoryId !== prevProps.categoryId) {
            if (this.props.searched) {
                this.props.dispatch(deleteData('search'));
            }
            this.fetchData();
        }
    }

    fetchData() {
        const {data, categoryId} = this.props;

        if (__isBrowser__ && !data) {
            CategoryItemList.data(categoryId).then((response) => {
                this.props.dispatch(saveData(categoryId, response));
            });
        }
    }



    render() {
        const {data, categoryId} = this.props;

        if (!data) {
            return 'Loading. Please wait!';
        }

        return (
            <>
                <Row>
                    <Column>
                        <Search categoryId={categoryId} />
                    </Column>
                    <Column shrink>
                        <span>{data.count} results</span>
                    </Column>
                </Row>
                {data.results.length > 0 && (
                    <ul style={{margin: 0, padding: 0}}>
                        {data.results.map((item) => <CategoryItem key={item.url} category={categoryId} item={item} />)}
                    </ul>
                )}
                <CategoryNavigation
                    categoryId={categoryId}
                    data={data}
                />
            </>
        );
    }
}

CategoryItemList.propTypes = {
    categoryId: PropTypes.string.isRequired,
    searched: PropTypes.bool.isRequired,
    data: PropTypes.shape({
        next: PropTypes.string,
        previous: PropTypes.string,
        count: PropTypes.number.isRequired,
        results: PropTypes.array
    }).isRequired,
    dispatch: PropTypes.func.isRequired
};

const mapStateToProps = (state, props) => {
    const {categoryId} = props.match.params;
    const {search} = state;
    const data = search || state[categoryId];
    const searched = !!search;

    return {
        searched,
        categoryId,
        data
    };
};

export default connect(mapStateToProps)(withRouter(CategoryItemList));
