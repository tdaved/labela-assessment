import styled from 'styled-components';

const CategoryItemStyleContainer = styled.div`
    list-style: none;
    margin-bottom: 1.5rem;
`;

export default CategoryItemStyleContainer;