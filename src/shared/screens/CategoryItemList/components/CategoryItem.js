import React from 'react';
import PropTypes from 'prop-types';
import CategoryItemStyleContainer from '../CategoryItemStyleContainer';
import People from './People';
import Planet from './Planet';
import Film from './Film';
import Specimen from './Specimen';
import Vehicle from './Vehicle';
import Starship from './Starship';

const categoryToComponentMap = (categoryName) => {
    const map = {
        people: People,
        planets: Planet,
        films: Film,
        species: Specimen,
        vehicles: Vehicle,
        starships: Starship,
        default: () => null
    };

    if (map[categoryName]) {
        return map[categoryName];
    }

    return map.default;
};

const CategoryItem = ({category, item}) => {
    const Component = categoryToComponentMap(category);

    return (
        <CategoryItemStyleContainer>
            <Component item={item} />
        </CategoryItemStyleContainer>
    );
};

CategoryItem.propTypes = {
    category: PropTypes.string.isRequired,
    item: PropTypes.object.isRequired
};

export default CategoryItem;
