import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {saveData} from '~/shared/actions';
import {Button} from '~/shared/components/Button';
import CategoryNavigationStyleContainer from './CategoryNavigationStyleContainer';

class CategoryNavigation extends React.Component {
    fetchPage(direction = 1) {
        const {categoryId, data} = this.props;
        const url = direction > 0 ? data.next : data.previous;

        fetch(url).then(response => response.json()).then((response) => {
            this.props.dispatch(saveData(categoryId, response));
        });
    }

    render() {
        const {data} = this.props;

        return (
            <CategoryNavigationStyleContainer>
                {data.previous ? <Button onClick={() => this.fetchPage(-1)}>Previous</Button> : <div />}
                {data.next && <Button onClick={() => this.fetchPage(1)}>Next</Button>}
            </CategoryNavigationStyleContainer>
        );
    }
}

CategoryNavigation.propTypes = {
    categoryId: PropTypes.string.isRequired,
    data: PropTypes.shape({
        next: PropTypes.string,
        previous: PropTypes.string
    }).isRequired,
    dispatch: PropTypes.func.isRequired
};

export default connect()(CategoryNavigation);
