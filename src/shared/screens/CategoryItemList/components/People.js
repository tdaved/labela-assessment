import React from 'react';
import PropTypes from 'prop-types';
import ItemName from './ItemName';
import ItemDetails from './ItemDetails';

const People = ({item}) => (
    <div>
        <ItemName>
            {item.name}
        </ItemName>
        <ItemDetails>
            {[
                `born in ${item.birth_year}`,
                item.gender,
                `${item.height} cm`,
                `${item.mass} kg`,
                `${item.hair_color} hair`
            ].join(' - ')}
        </ItemDetails>
    </div>
);

People.propTypes = {
    item: PropTypes.shape({
        name: PropTypes.string.isRequired,
        birth_year: PropTypes.string.isRequired,
        gender: PropTypes.string.isRequired,
        height: PropTypes.string.isRequired,
        mass: PropTypes.string.isRequired,
        hair_color: PropTypes.string.isRequired
    }).isRequired
};

export default People;