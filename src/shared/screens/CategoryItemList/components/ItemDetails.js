import styled from 'styled-components';

const ItemDetails = styled.div`
    font-size: 0.75rem;
    text-transform: uppercase;
    line-height: 1rem;
    color: rgba(0, 0, 0, 0.38);
`;

export default ItemDetails;