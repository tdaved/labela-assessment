import React from 'react';
import PropTypes from 'prop-types';
import ItemName from './ItemName';
import ItemDetails from './ItemDetails';

const Specimen = ({item}) => (
    <>
        <ItemName>
            {item.name}
        </ItemName>
        <ItemDetails>
            {[
                `classification: ${item.classification}`,
                `designation: ${item.designation}`,
                `${item.average_height} cm average height`,
                `language: ${item.language}`,
            ].join(' - ')}
        </ItemDetails>
    </>
);

Specimen.propTypes = {
    item: PropTypes.shape({
        name: PropTypes.string.isRequired,
        classification: PropTypes.string.isRequired,
        designation: PropTypes.string.isRequired,
        average_height: PropTypes.string.isRequired,
        language: PropTypes.string.isRequired
    }).isRequired
};

export default Specimen;