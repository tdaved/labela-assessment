import React from 'react';
import PropTypes from 'prop-types';
import ItemName from './ItemName';
import ItemDetails from './ItemDetails';

const Starship = ({item}) => (
    <>
        <ItemName>
            {item.name}
        </ItemName>
        <ItemDetails>
            {[
                `${item.model}`,
                `created by ${item.manufacturer}`,
                `class: ${item.starship_class}`,
                `${item.max_atmosphering_speed} max speed`,
            ].join(' - ')}
        </ItemDetails>
    </>
);

Starship.propTypes = {
    item: PropTypes.shape({
        name: PropTypes.string.isRequired,
        model: PropTypes.string.isRequired,
        manufacturer: PropTypes.string.isRequired,
        starship_class: PropTypes.string.isRequired,
        max_atmosphering_speed: PropTypes.string.isRequired
    }).isRequired
};

export default Starship;