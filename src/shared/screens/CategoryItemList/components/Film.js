import React from 'react';
import PropTypes from 'prop-types';
import ItemName from './ItemName';
import ItemDetails from './ItemDetails';

const Film = ({item}) => (
    <>
        <ItemName>
            {item.title}
        </ItemName>
        <ItemDetails>
            {[
                `episode: ${item.episode_id}`,
                `director: ${item.director}`,
                `producer: ${item.producer}`,
                `released in ${item.release_date}`,
            ].join(' - ')}
        </ItemDetails>
    </>
);

Film.propTypes = {
    item: PropTypes.shape({
        title: PropTypes.string.isRequired,
        episode_id: PropTypes.number.isRequired,
        director: PropTypes.string.isRequired,
        producer: PropTypes.string.isRequired,
        release_date: PropTypes.string.isRequired
    }).isRequired
};

export default Film;