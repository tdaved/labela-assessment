import styled from 'styled-components';

const ItemName = styled.div`
    margin-bottom: 0.5rem;
    font-size: 1.5rem;
    font-weight: bold;
    color: rgba(0, 0, 0, 0.60);
`;

export default ItemName;