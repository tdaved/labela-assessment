import React from 'react';
import PropTypes from 'prop-types';
import ItemName from './ItemName';
import ItemDetails from './ItemDetails';

const Vehicle = ({item}) => (
    <>
        <ItemName>
            {item.name}
        </ItemName>
        <ItemDetails>
            {[
                `${item.model}`,
                `created by ${item.manufacturer}`,
                `class: ${item.vehicle_class}`,
                `${item.max_atmosphering_speed} max speed`,
            ].join(' - ')}
        </ItemDetails>
    </>
);

Vehicle.propTypes = {
    item: PropTypes.shape({
        name: PropTypes.string.isRequired,
        model: PropTypes.string.isRequired,
        manufacturer: PropTypes.string.isRequired,
        vehicle_class: PropTypes.string.isRequired,
        max_atmosphering_speed: PropTypes.string.isRequired
    }).isRequired
};

export default Vehicle;