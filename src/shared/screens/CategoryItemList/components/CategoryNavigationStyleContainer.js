import styled from 'styled-components';

const CategoryNavigationStyleContainer = styled.div`
    display: flex;
    justify-content: space-between;
`;

export default CategoryNavigationStyleContainer;