import React from 'react';
import PropTypes from 'prop-types';
import ItemName from './ItemName';
import ItemDetails from './ItemDetails';

const Planet = ({item}) => (
    <>
        <ItemName>
            {item.name}
        </ItemName>
        <ItemDetails>
            {[
                `${item.rotation_period} hours a day`,
                `${item.orbital_period} days a year`,
                `${item.climate} climate`,
                `${item.terrain} terrain`,
            ].join(' - ')}
        </ItemDetails>
    </>
);

Planet.propTypes = {
    item: PropTypes.shape({
        name: PropTypes.string.isRequired,
        rotation_period: PropTypes.string.isRequired,
        orbital_period: PropTypes.string.isRequired,
        climate: PropTypes.string.isRequired,
        terrain: PropTypes.string.isRequired
    }).isRequired
};

export default Planet;