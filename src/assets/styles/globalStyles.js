import {injectGlobal} from 'styled-components';

injectGlobal`
    * {
        box-sizing: border-box;
    }
    body {
        min-width: 18rem;
        padding: 0 0 1rem;
        margin: 0;
        font-family: Arial, sans-serif;
        font-size: 18px;
        font-weight: 400;
        background-color: #eeeeee;
        line-height: 1.4rem;
    }
    h1 {
        font-size: 5em;
        margin: 0.67em 0;
    }
    h1, h2, h3, h4, h5, h6 {
        color: #001A33;
    }
`;