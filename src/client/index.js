import React from 'react';
import {Provider as ReduxProvider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import {hydrate} from 'react-dom';
import {App} from '~/shared/components/App';
import createStore from '~/shared/store';

/* Save store if provided by server */
export const store = createStore(window.__REDUX_DATA__ || {});
const app = document.getElementById('app');

hydrate((
    <ReduxProvider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </ReduxProvider>
), app);