const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const clientConfig = {
    mode: 'production',
    target: 'web',
    entry: './src/client/index.js',
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js',
        publicPath: '/'
    },
    resolve: {
        alias: {
            '~': path.resolve(__dirname, 'src'),
        }
    },
    module: {
        rules: [
            {test: /\.js$/, loader: 'babel-loader'}
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            __isBrowser__: "true"
        }),
        new CopyWebpackPlugin([{
            from: path.resolve(__dirname, 'src/assets/images'),
            to: ''
        }])
    ]
};

const serverConfig = {
    mode: 'production',
    target: 'node',
    externals: [nodeExternals()],
    entry: './src/server/index.js',
    output: {
        path: __dirname,
        filename: 'server.js',
        publicPath: '/'
    },
    resolve: {
        alias: {
            '~': path.resolve(__dirname, 'src'),
        }
    },
    module: {
        rules: [
            {test: /\.js$/, loader: 'babel-loader'}
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            __isBrowser__: "false"
        })
    ]
};

module.exports = [clientConfig, serverConfig];